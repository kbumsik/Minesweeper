#include <cstdlib>
#include <iostream>
#include "minesweeper.h"
using namespace std;

// !!--this is only for debugging
// Should be deleted in final stage
#define NO_MAP_TEST

//define the name of sample file
#define FILE_SAMPLE "sample.txt"

// macro to print a tile
#define TILE(STRING) " " << STRING << " "
#define FILL(STRING) STRING << STRING << STRING

// enum for game status
typedef enum status{
	startGame, viewStats, quitProgram
};
typedef enum click{
	leftClick, rightClick
};

status askWhatToDo()

void playTime(minesweeper& play);
void viewStatistics();
void printMineField(minesweeper &play);
void printTile(minesweeper &play, int x, int y);
click askForClick(minesweeper &play, int &x, int &y);
bool askToPlayAgain();
void askToLoadFromFile(minesweeper &play);
/*
 *
 */
int main() {
	// initialize a random seed
	srand(time(NULL));
	status options;
	minesweeper play;
     /* This will be your main GUI loop. You should query the user for actions such
     * as starting a game, viewing statistics, or quitting the program. You should
     * handle user input either succinctly or by making a call to a helper method
     * to perform the desired operations.
     */

    while (1) {
	//TODO - Ask the user with options to perform such as Starting a game, Viewing
	// statistics, Quitting the program,  and any
        // other functionality you feel is necessary.
        // Each iteration here is one round of the game.

        // somewhere you need to create a minesweeper game object first if the
        // user decides to start a new round of game, e.g.,
        // minesweeper play();
		options = askWhatToDo();
		switch (options)
		{
		case startGame:
			playTime(play);
			// !-- store statistics here
			system("cls");	// This is a function to clear console
			break;
		case viewStats:
			viewStatistics();
			break;
		case quitProgram:
			break;
		}
		if (options == quitProgram)
		{
			break;
		}
    }
	cout << "Program ended" << endl;
    return 0;
}

/* This one is to ask what to start
*/
status askWhatToDo(){
	int intIn;
	// get user input
	do
	{
		cout << "Choose one (Just type 1, 2 or 3): \n";
		cout << "1 - start game\n";
		cout << "2 - View Statistics\n";
		cout << "3 - Quit program\n";
		cin >> intIn;
	} while (intIn != 1 && intIn != 2 && intIn != 3);

	// return the program status
	switch (intIn)
	{
	case 1:
		// This is a function to clear console
		system("cls");
		return startGame;
	case 2:
		// This is a function to clear console
		system("cls");
		return viewStats;
	default:
		return quitProgram;
	}
}

/* This should be your game UI function. If a user decides to play a game of minesweeper,
* UI interaction with a minesweeper object should be done here. For example, the repeated
* printing of the board state and handling of user game action such as left/right click
* on a particular cell should be done here
*/
void playTime(minesweeper& play) {
//TODO - Begin the minesweeper game function; should interact with minesweeper class/object here
	click clickresult;
	int x, y;
	bool playagain = true;

	// loop while the user say not to play again
	while (playAgain)
	{
		// Ask to load from file
		system("cls");
		askToLoadFromFile(play);
		while (play.endGame() == ongoing)
		{
			system("cls");	// This is a function to clear console

			// Print the board
			printMineField(play);
			// Ask for a click
			clickResult = askForClick(play, x, y);

			// do the job
			switch (clickResult)
			{
			case leftClick:
				play.revealLocation(x, y);
				break;
			case rightClick:
				play.markLocation(x, y);
				break;
			default:
				break;
			}
		}

		// print the final board status
		system("cls");
		printMineField(play);
		// decide weather win or not
		if (play.endGame() == win)
		{
			cout << "You win!!!!!!!!!!!!!!" << endl;
		}
		else
		{
			cout << "YOU ARE LOOOOOOOOSSSSEEEERRRRRRR!!!!!!!!!!!!!!" << endl;
		}
		// wait until you press enter
		cin.clear();
		fflush(stdin);	// clear input
		cout << "please press enter..." << endl;
		getchar();

		// ask to play again
		system("cls");
		playAgain = askToPlayAgain();
	}

}

/* display the statistics of the game. This is a bonus point function
 */
void viewStatistics() {
    cout << "Currently Not Supported \n" << endl;
}




/*==========================================================
*	Functions for playTime();
*==========================================================*/

/* print the Mine Field of the game
*/
void printMineField(minesweeper &play){
    // print the first line
    cout << TILE(" ");
	cout << TILE("|");
    for(int i=0; i<play.getColNum();i++)
    {
        cout << TILE(i);
    }
    cout << endl;

	// print a seperator
	cout << FILL("-");
	cout << FILL("-");
	for (int i = 0; i<play.getColNum(); i++)
	{
		cout << FILL('-');
	}
	cout << endl;

    // print the rest of lines
    for(int i=0; i<play.getRowNum();i++)
    {
        //first print the row number
        cout << TILE(i);
		cout << TILE("|");
        //then print the minefield
        for(int j=0; j <play.getColNum(); j++)
        {
			printTile(play, j, i);
        }
        // go next line
        cout<<endl;
    }
}

/* get a character of a tile
*/
void printTile(minesweeper &play, int x, int y){
    // when a tile is hidden
#ifdef MAP_TEST
    if(play.isRevealed(x,y))
#else
	if (!play.isRevealed(x, y))
#endif
    {
        // if flagged, get 'F'
        if(play.isFlagged(x,y))
        {
            cout <<TILE('F');
        }
        // get '*'by default
        else
        {
            cout <<TILE('*');
        }
    }
    // when the tile is revealed

#ifdef MAP_TEST
    else if(!play.isRevealed(x,y))
#else
	else if (play.isRevealed(x, y))
#endif
    {
        // get the value of tile anyway
        if(play.valueOf(x,y)==mine)
        {
            cout <<TILE('X');
        }
        else if(play.valueOf(x,y)==no_mine)
        {
            cout <<TILE('-');
        }
        else
        {
            cout <<TILE(play.valueOf(x,y));
        }
    }
    // nothing by default
    else
    {
        cout <<TILE(' ');
    }
}

/* get click from user
*/
click askForClick(minesweeper &play, int &x, int &y){
	char mouse;
	bool rightInput = false;
	while (!rightInput)
	{
		cin.clear();
		fflush(stdin);	// clear input

		cout << "Click a tile you want ex) r,1,2 or l,5,6" << endl;
		// get mouse
		cin >> mouse;

		if (mouse != 'r'&&mouse != 'l')
		{
			cout << "Worng input:";
			cout << "must be 'r' or 'l'\n" << endl;
			continue;
		}
		getchar();		//ignor ','

		// get x input
		cin >> x;
		if (x<0 || x> (play.getRowNum() - 1))
		{
			cout << "Worng input:";
			cout << "x must be between 0 and " << play.getRowNum() - 1 << endl;
			continue;
		}
		getchar();		//ignor ','

		// get y input
		cin >> y;
		if (y<0 || y>(play.getColNum() - 1))
		{
			cout << "Worng input:";
			cout << "y must be between 0 and " << play.getColNum() - 1 << endl;
			continue;
		}

		cin.clear();
		fflush(stdin);	// clear input

        //determine left or right click
        switch(mouse)
        {
        case 'r':
            return rightClick;
        case 'l':
            return leftClick;
        default:    //error!
            cout<<"Error caused"<<endl;
            exit (EXIT_FAILURE);
        }
	}

}

/* ask user to play again
*/

bool askToPlayAgain(){
	char charIn;
	while (true)
	{
		cout << "Do you want to play again? y/n" << endl;
		cin >> charIn;
		switch (charIn)
		{
		case 'y':
			return true;
		case 'n':
			return false;
		default :
			continue;
		}
	}
}

/* ask user to load the mine from file or not
*/
void askToLoadFromFile(minesweeper &play){
	char charIn;
	bool isOK = false;
	while (!isOK)
	{
		cout << "Do you want to play with the sample file? y/n" << endl;
		cin >> charIn;
		switch (charIn)
		{
		case 'y':
			//initialize the game by sample file
			isOK = true;
			play.initialMineField(FILE_SAMPLE);
			break;
		case 'n':
			isOK = true;
			//initialize the game by default
			play = minesweeper();
			break;
		default:
			continue;
		}
	}
}
