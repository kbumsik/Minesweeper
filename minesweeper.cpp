#include "minesweeper.h"

// =====================
// define macros
// =====================

// absolute value macro
#define ABS(VALUE)	((VALUE>=0)?VALUE:-(VALUE))
// check if a value in range
#define GET_X(VALUE, SIZE_X) (VALUE % SIZE_X)
#define GET_Y(VALUE, SIZE_Y) (VALUE / SIZE_Y)

using namespace std;

minesweeper::minesweeper() {
	// TODO - default two dimension array is 9 X 9 with 10 mines

	colNum = DEFAULT_NUM_OF_COL;
	rowNum = DEFAULT_NUM_OF_ROW;
	minesNum = DEFAULT_NUM_OF_MINE;
	end = ongoing;
	// generate fields first
	// generate a vector of mines randomly using randomPick
	// call calculateSurronding
	// so, call initialMineField
	initialMineField(DEFAULT_NUM_OF_COL, DEFAULT_NUM_OF_ROW);
}

minesweeper::minesweeper(int col, int row, int numOfMines) {
	// TODO Auto-generated constructor stub
	// TODO - two dimension gameboard size col x num with numOfMines mines

	// initialize private class members
	colNum = col;
	rowNum = row;
	minesNum = numOfMines;
	end = ongoing;
	// generate fields first
	// generate a vector of mines randomly using randomPick
	// call calculateSurronding
	// so, call initialMineField
	initialMineField(col, row);
}

minesweeper::~minesweeper() {
	// TODO Auto-generated destructor stub
}

/**
 * Returns int representing number of rows of current playing field.
 *
 * @return      Number of rows on playing field
 * */
int minesweeper::getRowNum()
{
	return this->rowNum;
}

/**
 * Returns int representing number of columns of current playing field.
 *
 * @return      Number of rows on playing field
 * */
int minesweeper::getColNum()
{
	return this->colNum;
}
/**
 * Returns int representing number of mines on current playing field.
 * Includes both hidden and revealed mines.
 *
 * @return      Number of rows on playing field
 * */
int minesweeper::getMinesNum()
{
	return this->minesNum;
}

/**
 * Returns int randomly generated between 0 and num
 *
 * @param   num Upper limit of randomly generated number
 * @return      Number of rows on playing field
 * @see         initialMineField()
 * */
int minesweeper::randomPick(int num)
{
	
	// return the random value
	return rand() % num;
}

/**
 * Generates mine field after accepting player's first turn.
 * Does not have to avoid first turn game over. 
 *
 * @param   fpX X-coordinate of first pick
 * @param   fpY Y-coordinate of first pick
 * @see     calculateSurrounding()
 * @see     randomPick()
 * */
void minesweeper::initialMineField(int fpX, int fpY) {
	//Generate our map and mask
	//TODO - generate all mines randomly
	//surrounding tile values should be updated to reflect presence of adjacent mine

	// generate the empty vector field first
	mineField = vector<vector<int>>(fpY, vector<int>(fpX, no_mine));
	bitField = vector<vector<bool>>(fpY, vector<bool>(fpX, hidden));
	flagField = vector<vector<bool>>(fpY, vector<bool>(fpX, unflagged));

	// generate mines randomly
	// c means coordinate
	int c;
	for (int i = 0; i < this->minesNum; i++)
	{
		// generate a random coordinate of a mine
		c = randomPick(fpX*fpY);
		// if the tile is NOT already mine, make it mine
		if (mineField[GET_Y(c, getRowNum())][GET_X(c, getColNum())] != mine)
		{
			mineField[GET_Y(c, getRowNum())][GET_X(c, getColNum())] = mine;
		}
		// if it does, do not make it mine and iterate again
		else
		{
			i--;
			continue;
		}
	}

	// update all number of surrounding mines
	for (int i= 0; i < mineField.size();i++)
	{
		for (int j =0; j <mineField[i].size(); j++)
		{
			calculateSurrounding(i,j);
		}
	}

}

/**
 * Generates mine field based on file. This option
 * does not have to check the user's first turn for 1-turn game over.
 *
 * @param   path to load MineField from
 * */
void minesweeper::initialMineField(string path) { 
	//TODO - load mine field from path
	//Is not concerned with user's first tile
	// initialize local variable
	
	ifstream mineFile(path.c_str());
	if (mineFile.fail())
	{
		cerr << "File does not exist";
		exit(1);
	}
	// get x size
	mineFile >> this->colNum;
	// get y size
	mineFile >> this->rowNum;
	// get number of mines
	mineFile >> this->minesNum;

	// generate the empty vector field first
	mineField = vector<vector<int>>(this->rowNum, vector<int>(this->colNum, no_mine));
	bitField = vector<vector<bool>>(this->rowNum, vector<bool>(this->colNum, hidden));
	flagField = vector<vector<bool>>(this->rowNum, vector<bool>(this->colNum, unflagged));


	// add mines
	for (int i = 0; i < this->minesNum; i++)
	{
		int x, y;
		//skip empty charactor three times;
		mineFile.get();
		mineFile.get();
		mineFile.get();
		mineFile >> x >> y;
		mineField[y][x] = mine;
	}

	// update all number of surrounding mines
	for (int i = 0; i < mineField.size(); i++)
	{
		for (int j = 0; j <mineField[i].size(); j++)
		{
			calculateSurrounding(i, j);
		}
	}
}

/**
 * Generates numbers for surrounding tiles of mines. The only
 * tiles with numbers are those surrounding mines; these tiles are
 * updated as mines are generated.

 *
 * @param   row Row number of generated mine
 * @param   col Column number of generated mine
 * @see     initialMineField()
 * */
void minesweeper::calculateSurrounding(int row, int col) {
	//TODO - should update surrounding tiles to reflect
	//presence of adjacent mine
	
	// if it does not have mine, just escape
	if(mineField[row][col]!=mine)
	{
		return;
	}
	// if it is mine, update surronding
	else
	{
		//check in clockwise order
		//add value only if it is not mine and in range of the vector
		// 12 o'clock
		if(row > 0)
		{
			if(mineField[row-1][col]!=mine)
				{
					++mineField[row-1][col];
				}
		}
		// 1 o'clock
		if (row > 0 && col < (getColNum() - 1))
		{
			if (mineField[row - 1][col + 1] != mine)
			{
				++mineField[row - 1][col + 1];
			}
		}
		// 3 o'clock
		if(col < (getColNum()-1))
		{
			if(mineField[row][col+1]!=mine)
				{
					++mineField[row][col+1];
				}
		}
		// 4 o'clock
		if (col < (getColNum() - 1) && row < (getRowNum() - 1))
		{
			if (mineField[row+1][col + 1] != mine)
			{
				++mineField[row+1][col + 1];
			}
		}
		// 6 o'clock
		if(row < (getRowNum()-1))
		{
			if(mineField[row+1][col]!=mine)
				{
					++mineField[row+1][col];
				}
		}
		// 7 o'clock
		if (row < (getRowNum() - 1) && col > 0)
		{
			if (mineField[row + 1][col-1] != mine)
			{
				++mineField[row + 1][col-1];
			}
		}
		// 9 o'clock
		if(col > 0)
		{
			if(mineField[row][col-1]!=mine)
				{
					++mineField[row][col-1];
				}
		}
		//11 o'clock
		if (col > 0 && row > 0)
		{
			if (mineField[row-1][col - 1] != mine)
			{
				++mineField[row-1][col - 1];
			}
		}
	}
}

/**
 * Updates bitField with user selection of revealed location.
 * Reveals current location and immediately checks mineField
 * for mine. Recursively calls helper function unmask for blank
 * tiles. 
 *
 * @param   row Row number of user selected tile
 * @param   col Column number of user selected tile
 * @see         unmask();
 * */
void minesweeper::revealLocation(int x, int y) {
	//TODO - update the bitField to reflect the user's
	//tile selection. Should rely on unmask to do actual
	//reveal operation.

	// if the tile is flagged or already shown, do nothing.
	if(this->flagField[y][x]==flagged || this->bitField[y][x]==shown)
	{
		return;
	}
	// if it is not flagged, try unmask
	else
	{
		unmask(y, x);
	}
}

/**
 * Allow the user to mark a cell as a potential mine. Marking an already
 * marked cell will remove the mark for that cell
 *
 * @param x column number of the cell selected
 * @param y row number of the cell selected
 */
void minesweeper::markLocation(int x, int y) {
    // TODO - update the display for the selected cell, change it
    // to marked if it's not marked, or to unmarked if it's already marked
	if (this->flagField[y][x] != flagged)
	{
		this->flagField[y][x] = flagged;
	}
	else
	{
		this->flagField[y][x] = unflagged;
	}
}

/**
 * Checks end game status (int end). End -1 is loss condition.
 * Also checks onlyMines() for a win condition. onlyMines will
 * update the end game status and endGame should return this value.
 *
 * @return      end game status (-1 loss, 0 ongoing, 1 win)
 * @see         onlyMines();
 * @see		revealLocation();
 * */
int minesweeper::endGame() {
	//TODO - return current end game status.
	//Calling this method should only update int end with
	//win or ongoing condition. revealLocation should
	//update if there is loss condition.
	onlyMines();
	return end;
}

/**
 * Checks end game status (int end). End 1 is win condition;
 * end 0 is ongoing. onlyMines will only update end with these
 * two conditions. onlyMines will check the entire playing field
 * (does not rely on numMines or any internal counter). 
 *
 * @see         endGame();
 * */
void minesweeper::onlyMines() {
	//TODO - check for win condition, otherwise ongoing
	bool matched = false;
	// if there is revealed mine, you loose
	// if there is only mines. you win
	// otherwise, ongoing
	for(int i=0; i< this->getRowNum(); i++)
	{
		for(int j=0; j< this->getColNum(); j++)
		{
			// if there is shown and mine, you loose.
			// and terminate this function
			if (this->bitField[i][j] == shown)
			{
				if(this->mineField[i][j]==mine)
				{
					this->end = loss;
					return;
				}
			}
			// if there is hidden and not mine, you are ongoing
			else if (this->bitField[i][j] == hidden)
			{
				if(this->mineField[i][j] != mine)
				{
					matched = true;
				}
			}
		}
	}
	if(matched)
	{
		this->end = ongoing;
	}
	else
	{
		// if there is no occurrance, you win
		this->end = win;
	}
}
	


/**
 * Checks if the selected position should be visible
 *
 * @param   row Row number of user selected tile
 * @param   col Column number of user selected tile
 * @see         valueOf();
 * */
bool minesweeper::isRevealed(int x, int y) {
	//TODO - check if a user has revealed a specific tile
	return !(this->bitField[y][x]);
}

/**
 * Checks if the value of selected tile. isRevealed
 * should be called before this for correct gameplay. 
 * 
 * @param   row Row number of user selected tile
 * @param   col Column number of user selected tile
 * @see         isRevealed();
 * */
int minesweeper::valueOf(int x, int y) {
	//TODO - returns the value of specific tile.
	//should only be called if tile isRevealed.
	return this->mineField[y][x];
}

/**
 * Reveals surrounding tiles. Should only be called if
 * user selected tile was not mine or had no surrounding
 * mines. Will recursively call itself to reveal all
 * adjacent blank tiles.
 *
 * @param   row Row number of user selected tile
 * @param   col Column number of user selected tile
 * @see         revealLocation();
 * */
void minesweeper::unmask(int row,int col) {
	//TODO - reveal the tile here.
	//This method should reveal surrounding tiles
	//if the tile revealed had a value of 0

	// this function is a recursive function.

	// escape condition 1: if this tile  already shown, escape
	if (this->bitField[row][col] == shown)
	{
		return;
	}
	// show this file first
	if (this->flagField[row][col] != flagged)
	{
		this->bitField[row][col] = shown;
	}
	// escape condition 2: if this tile is not no_mine, or already shown, escape
	if (this->mineField[row][col] != no_mine)
	{
		return;
	}
	else
	{
		//do it in clockwise order, recursive.
		// 12 o'clock
		if (row > 0)
		{
			this->unmask(row - 1, col);
		}
		// 1 o'clock
		if (row > 0 && col < (getColNum() - 1))
		{
			this->unmask(row - 1, col + 1);
		}
		// 3 o'clock
		if (col < (getColNum() - 1))
		{
			this->unmask(row, col + 1);
		}
		// 4 o'clock
		if (col < (getColNum() - 1) && row < (getRowNum() - 1))
		{
			this->unmask(row + 1, col + 1);
		}
		// 6 o'clock
		if (row < (getRowNum() - 1))
		{
			this->unmask(row + 1, col);
		}
		// 7 o'clock
		if (row < (getRowNum() - 1) && col > 0)
		{
			this->unmask(row + 1, col - 1);
		}
		// 9 o'clock
		if (col > 0)
		{
			this->unmask(row, col - 1);
		}
		//11 o'clock
		if (col > 0 && row > 0)
		{
			this->unmask(row - 1, col -1);
		}
	}
}


/**
 * Returns int representing flag status
 *
 * @return      if the tile is flagged
 * */
bool minesweeper::isFlagged(int x, int y){
	return this->flagField[y][x];
}
